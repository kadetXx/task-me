import React, { Component } from 'react'
import TodoItem from './TodoItem'
import PropTypes from 'prop-types'
import './Todos.css'
import NewTodo from './NewTodo'

export class Todos extends Component {
  render() {
    return (
      <div>
      <NewTodo addTodo={this.props.addTodo} />

      {this.props.todos.map((todo) => ( <TodoItem key={todo.id} todo={todo} markComplete={this.props.markComplete} delTodo={this.props.delTodo} /> ))}
      </div>
    )
  }
}

Todos.propTypes = {
  todos: PropTypes.array.isRequired,
  markComplete: PropTypes.func.isRequired,
  delTodo: PropTypes.func.isRequired
}

export default Todos
