import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class TodoItem extends Component {

  render() {

    const {title, uniqueId} = this.props.todo;

    return (
      <div>
        <p style={this.getStyle()}>
          <input className='check' type="checkbox" onChange={this.props.markComplete.bind(this, uniqueId)} />
          {title}
          <button style={this.styleBtn} onClick={this.props.delTodo.bind(this, uniqueId)} ><i className="fas fa-times"></i></button>
        </p>
      </div>
    )
  }

  getStyle() {
    return {
      padding: '10px 20px',
      margin: '0',
      textDecoration: this.props.todo.completed ? 'line-through' : 'none',
      color: this.props.todo.completed ? 'grey' : '',
      background: this.props.todo.completed ? 'darkseagreen' : 'lightgrey',
      borderBottom: '1px solid grey',
    }
  }

}

TodoItem.propTypes = {
  todo : PropTypes.object.isRequired
}
export default TodoItem
