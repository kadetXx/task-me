import React, { Component } from 'react'

export class About extends Component {


  render() {

    const style = {
      padding: '4%'
    }

    const pstyle = {
      lineHeight: '1.5',
      color: '#454343'
    }

    return (
      <React.Fragment>
        <section style={style}>
          <h1>About Page</h1>
          <p style={pstyle}>TaskMe is a simple todo list application built with react js. I built this after learning from traversy media's react crash course. Now I'm off to build my portfolio, with react!</p>
        </section>
      </React.Fragment>
    )
  }
}

export default About
