import React from 'react'
import { Link } from 'react-router-dom'

export default function header() {

  const styling = {
    textAlign: 'center',
    margin: '0',
    padding: '2% 20px',
    display: 'flex',
    borderBottom: '1px solid grey'
  }

  const head = {
    margin: '0',
    color: '#2b2a2a'
  }

  const nav = {
    display: 'flex',
    alignItems: 'center'
  }

  const link = {
    marginLeft: '1rem'
  }

  return (
    <div style={styling}>
      <h2 style={head}><i class="fas fa-clipboard-list"></i> TaskMe</h2>
      <div style={nav}>
        <Link style={link} to='/'>Home</Link>
        <Link style={link} to='/about'>About</Link>
      </div>
    </div>
  )
}
