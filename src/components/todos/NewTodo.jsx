import React, { Component } from 'react'

export class NewTodo extends Component {

  state = {
    title: ''
  }

  onChange (e) {
    this.setState( { [e.target.name]: e.target.value } )
  }

  submit (e) {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState({title: ''});
  }


  render() {
    return (
      <div>
        <form onSubmit={this.submit.bind(this)}>
          <label>
            <input type="text" name='title' placeholder='Add new item' value={this.state.title} onChange={this.onChange.bind(this)} />
            <button type="submit">+ Add</button>
          </label>
        </form>
      </div>
    )
  }
}

export default NewTodo
