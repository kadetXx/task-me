import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import Todos from './components/todos/Todos'
import Header from './components/layout/header'
import uuid from 'react-uuid';
import axios from 'axios'
import About from './pages/About'

export class App extends Component {

  state = {
    todos: [
      {
        uniqueId: uuid(),
        title: 'Prepare breakfast for mum',
        completed: false
      },

      {
        uniqueId: uuid(),
        title: 'Visit the gym and workout',
        completed: false
      },

      {
        uniqueId: uuid(),
        title: 'Redesign my portfolio site',
        completed: false
      }
    ]
  }

  // componentDidMount () {
  //   axios.get('https://jsonplaceholder.typicode.com/todos?_limit=3')
  //   .then(res => this.setState( { todos: res.data } ))
  // }

  markComplete(id) {
    this.setState({todos: this.state.todos.map(todo => {
      if (todo.uniqueId === id) {
        todo.completed = !todo.completed
      }

      return todo
    }) })
  }

  delTodo(id) {
    this.setState({todos: this.state.todos.filter(todo => todo.uniqueId !== id) })
  }

  addTodo(title) {
    // const newTodo = {
    //   id: uuid(),
    //   title,
    //   completed: false
    // }
    axios.post('https://jsonplaceholder.typicode.com/todos', {
      uniqueId: uuid(),
      title,
      completed: false
    })
    .then(res => this.setState({ todos: [...this.state.todos, res.data] }))

    // this.setState({ todos: [...this.state.todos, newTodo] })
  }

  render() {
    return (
      <Router>
        <div className="App">
          <Header />

          <Route exact path='/' render={props => (
            <Todos todos={this.state.todos} markComplete={this.markComplete.bind(this)} delTodo={this.delTodo.bind(this)} addTodo={this.addTodo.bind(this)} />
          )} />

          <Route exact path='/about' component={About} />
        </div>
      </Router>
    )
  }
}

export default App
